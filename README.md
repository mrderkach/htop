
# README #

### HOW TO ###

The program displays current processes (internal are hidden).

UI:
 
* Use Arrow Keys / Page Up, Page Down to navigate. 
* Press double 'k' to kill the chosen process.
* Press space to pause/resume process.
* Press '1' to '5' to sort processes. Double press to change the direction.
('1' - by id, '2' - by name, '3' - by status, '4' - by virt, '5' - by rss)
* Press 'q' or F1 to exit.

### Installation ###

* Makefile is located in the root
* ./htop to start the Process Manager

### Created by ###

Denis Derkach,
ami142 CS HSE
Moscow, 2015

# Report #

### Goal ###

Implement task manager for Ubuntu, similar to htop program and Windows task manager.

---

Реализовать менеджер процессов, аналогичный по функциональности диспетчеру задач Windows или программе htop в Linux.

**Criterias:**

1. Implement the task manager, that would be able to display the list of processes and kill the chosen.
2. Support the list sorting based on several parameters in both directions. 
3. Support the pause/continue signals, display extended info about the processes.
4. Support of displaying the table of virtual memory of process.
---
1. Реализация менеджера процессов, который отображает список процессов и позволяет снять с выполнения любой процесс.
2. Поддержка сортировки списка процессов по нескольким критериям.
3. Поддержка приостановки/продолжения выполнения процессом и просмотра расширенной информации о состоянии процесса.
4. Поддержка таблицы отображения виртуального адресного пространства процесса.

### Implementation ###

**Technologies**

The development of the process manager was based on the ncurses package (terminal input/output). It was extended with the menu package, which provides a set of functions for creating a menu. For collecting information about the processes the file system /proc used.

---

Для реализации менеджера процессов использовалась библиотека терминального ввода/вывода ncurses, расширенная библиотекой menu, предоставляющей набор функций для создания меню. Для сбора информации о процессах потребовалось освоение файловой системы /proc.

**Problems and solutions**

1. Dirent library was used to go through file system. To collect process info I had to parse cmdline and status in /proc.
2. When you start the program, ncurses draws a window with the name of program and navigation info. Inside, you'll also see another windows with menu package set up, which shows processes and continually refreshes.
3. Initial way of refreshing the list and waiting for user command was to tough for the processor, as it was implemented as infinite cycle with no sleep timer. Therefore the getch command was set up with the timer, so it waits for used command for 1 second, when fails and goes to the next iteration. Every 3 iterations the list of processes refreshes.
4. To stop, pause and continue any process the program uses kill command with the appropriate signal parameter.
5. There was a significant problem because of memory leaks. So, some C structures were replaced with the equialent C++ structures, and overall some funcations were rewritten more accurately.
6. Make was used with the automatically generated dependecies via deps.make.
---
1. Для обхода файловой системы /proc использовались функции библиотеки dirent. Для сбора информации о процессах для каждого процесса потребовалось распарсить два файла - cmdline и status. 
2. Отображение процессов на экран реализовано следующим образом: при старте программы создается окно с помощью ncurses, в котором отображается только общая информация - имя программы, способы навигации. В этом окне также расположено внутреннее окно, для которого настроено menu, в котором отображается список процессов и которое постоянно обновляется.
3. Первоначально обновление процессов и ожидание пользовательский действий было реализовано следующим образом: бесконечный цикл, в каждой итерации которого запускалась функция getch, которая сразу же выдавала ошибку, если пользователь не вводил никаких команд, либо возвращала код команды. Раз в 1000 итераций обновлялся список процессов. Такая реализация привела к постоянной загрузке процессора. Поэтому, первоначальный способ реализации был модифирован: в getch была установлена задержка в 1 секунду. Таким образом, в течение этого времени программа исключительно ожидает команды от пользователя, после чего обновляется список процессов. Если же пользователь вводит команды, то в случае навигации список процессов обновляется один раз в 3 команды, в остальных случаях - сразу же после выполнения введенной команды.
4. Для завершения, приостановки/возобновления процесса в программе используется команда kill, посылающая необходимый сигнал.
5. В процессе борьбы с утечками памяти в некоторых случаях было написано более аккуратное осовобождение памяти, а также некоторые струтуры языка С были заменены на аналогичные языка С++.
6. Для удобства для создания исполняемого файла использовалась утилита make (с автоматическим построением зависимостей с помощью deps.make).

### Results ###

Process manager was developed, which shows the list of processes. It refreshes every second in average. You can navigate across the list with the key arrows and PageUp, PageDown buttons. For every process there are 5 parameters: id, name, status, Resident Set Size(RSS) & Virtual Memory Size (VIRT). Every parameter supports sorting in both directions. By double-pressing "k" you can send the "TERM" signal. You can pause ("SIGSTOP" signal) and continue ("SIGCONT") every process by pressing the space.

In the root directory you will find the following files: Makefile, executable file after calling make, README, dependancy file deps.make (auto-generated), and the src folder. The folder contains main.cpp, which only starts the main program, win.h with all the headers and win.cpp with all the functions and structures. 

---

Был реализован менеджер процессов, отображающий список процессов, обновляемый, в среднем, 1 раз в секунду. По списку процессов реализована навигация с помощью клавиатурных стрелок, а также PageUp, PageDown. Для каждого процесса в списке отображается 5 параметров: id, имя, статус, Resident Set Size(RSS) и Virtual Memory Size (VIRT). По каждому параметру поддерживается сортировка списка процессов как по убыванию, так и по возрастанию. Каждому процессу может быть послан сигнал завершения TERM при нажатии клавиши 'k' дважды. Каждый процесс может быть также приостановлен (сигнал SIGSTOP) и возобновлен (сигнал SIGCONT) по нажатию на пробел.

В директории располагаются следующие файлы: Makefile, собирающий исполняемый файл при вызове make, README, файл зависимостей deps.make, автоматически генерируемый при вызове make, а также папка src. В этой папке находятся: main.cpp - программа, запускающая главную функцию и не несущая смысловой нагрузки, win.h со всеми заголовками и win.cpp с кодом всех функций и структур.

** Overall **

1. Process manager was developed within the given requirements.
2. Thus, goals 1, 2, 3 were achieved.
---
1. Реализован менеджер процессов с некоторыми возможностями и функционалом.
2. Выполнены пункты задания 1, 2, 3.