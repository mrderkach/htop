#include "./win.h"
#include <iostream>
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

const int DELAY = 10;
const int COMMANDS_IN_ROW = 3;
const char *PATH = "/proc/";
const int EMPTYLINES = 8;

void DrawBorder(WINDOW *win, int row, int column) {

    box(win, 0, 0);
}

Process::Process(int id, std::string name, std::string status, int virt, int rss, std::string display):
    id(id),
    name(name),
    status(status),
    virt(virt),
    rss(rss),
    display(display)
{}

SortProcesses::SortProcesses(int sortBy):
    sortBy(sortBy)
{}

bool SortProcesses::operator() (const Process &self, const Process &other) {
    switch (sortBy) {
        case -1:
            return self.id > other.id;
        case 2:
            return self.name < other.name;
        case -2:
            return self.name > other.name;
        case 3:
            return self.status < other.status;
        case -3:
            return self.status > other.status;
        case 4:
            return self.virt < other.virt;
        case -4:
            return self.virt > other.virt;
        case 5:
            return self.rss < other.rss;
        case -5:
            return self.rss > other.rss;
    }
    //sortBy == 1
    return self.id < other.id;
}

// Returns vector of active processes (Internal are not included)
std::vector <Process> RefreshProcesses(int sortBy) {
    std::vector <Process> processes;
    DIR *dir = opendir(PATH);

    if (!dir) return processes;

    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) {
        struct stat sb;
        std::string currentFile = std::string(PATH) + "/" + dp->d_name;

        if (lstat(currentFile.c_str(), &sb) == 0) {
            if (!S_ISDIR(sb.st_mode)) {
                continue;
            } 

            char* ends;
            int id = static_cast <int> (strtol(dp->d_name, &ends, 10));

            if (*ends != 0) {
                continue;
            }

            std::string currentName;
            std::string fileProcessName = currentFile + "/cmdline";
            std::ifstream cmdline(fileProcessName);
            cmdline >> currentName;

            if (currentName.size() != 0) {
            
                std::string currentState(".");
                int currentVIRT = -1, currentRSS = -1;
                std::string fileStatusName = currentFile + "/status";
                std::ifstream status(fileStatusName);
                for (int i = 0; i < 17; ++i) {
                    if (i == 1) { //State
                        std::string tr;
                        status >> tr >> currentState;
                    } else if (i == 11) { //VIRT
                        std::string tr;
                        status >> tr >> currentVIRT;
                    } else if (i == 16) { //RSS
                        std::string tr;
                        status >> tr >> currentRSS;
                    }
                    std::string line;
                    getline(status, line);
                }

                char buffer[200];
                sprintf(buffer, "%-5d   %-40.40s   %-5.5s   %-7d   %-7d", id, currentName.c_str(), currentState.c_str(), currentVIRT, currentRSS);
                std::string display(buffer);

                processes.push_back(Process(id, currentName, currentState, currentVIRT, currentRSS, display));
            }
        }
    }

    sort(processes.begin(), processes.end(), SortProcesses(sortBy));
    closedir(dir);

    return processes;
}


void Start() {

// Preparing new screen
    initscr();
    
    keypad(stdscr, TRUE);

    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    init_pair(2, COLOR_WHITE, COLOR_BLACK);
    init_pair(3, COLOR_BLACK, COLOR_WHITE);
    cbreak();
    noecho();
    bkgd(COLOR_PAIR(1));

    std::vector <ITEM *> menuProcesses(1);
    menuProcesses[0] = NULL;

// Processes will be displayed through Menu functionality
    MENU *mainMenu = new_menu(const_cast <ITEM **> (menuProcesses.data()));
    set_menu_fore(mainMenu, COLOR_PAIR(2) | A_REVERSE);
    set_menu_back(mainMenu, COLOR_PAIR(1));

    int rows = 0, cols = 0;
    getmaxyx(stdscr, rows, cols);
    
// Sub window is created to display processes there.    
    WINDOW *menuscr = newwin(rows - 7, cols - 2, 4, 1);
    keypad(menuscr, TRUE);

    wbkgd(menuscr, COLOR_PAIR(1));
    wrefresh(menuscr);

    set_menu_win(mainMenu, menuscr);
    set_menu_sub(mainMenu, derwin(menuscr, 0, 0, 0, 0));
    set_menu_format(mainMenu, rows - EMPTYLINES, 1);
    set_menu_mark(mainMenu, " * ");

    DrawBorder(stdscr, rows, cols);

// Some common information is printed on the screen  
    const char * projectTitle = "HTOP project";
    mvprintw(1, (cols - sizeof(projectTitle) - 4) / 2, projectTitle);
    attron(COLOR_PAIR(2));
    mvprintw(3, 4, "PID      Command                                Status     Virt      RSS     ");
    attron(COLOR_PAIR(3));
    mvprintw(LINES - 3, 2, "Double 'k' to kill. Number of row to sort. Space to hold/resume.");
    mvprintw(LINES - 2, 2, "Press q or F1 to exit.                         (c) Denis Derkach");
    attroff(COLOR_PAIR(3));

    refresh();
    
    post_menu(mainMenu);
    wrefresh(menuscr);

/*  -----------------------------------
    Managing UI
    -----------------------------------
*/
    int sortProcesses = 1;
    int curComInRow = 0;
    bool needsRefreshing = true;
    bool killConfirm = false;
    std::vector <Process> processes; 
    halfdelay(DELAY);
    while (true) {

        int ch;
        if (needsRefreshing || curComInRow >= COMMANDS_IN_ROW || (ch = getch()) == ERR) {

/*          -----------------------------------
            Refreshing the list of processes on  the screen.
            -----------------------------------
*/
            needsRefreshing = false;
            curComInRow = 0;
            processes = RefreshProcesses(sortProcesses);            

            int hook_item = item_index(current_item(mainMenu));
            int hook_row = top_row(mainMenu);
            unpost_menu(mainMenu);
            for (auto elem: menuProcesses) {
                free_item(elem);
            }

            int menuSize = processes.size() + 1;
            menuProcesses.resize(menuSize);
            for (int i = 0; i < menuSize - 1; ++i) {
                menuProcesses[i] = new_item(processes[i].display.c_str(), "");
            }
            menuProcesses[menuSize - 1] = NULL;

            set_menu_items(mainMenu, const_cast <ITEM **> (menuProcesses.data())); 
            set_menu_format(mainMenu, rows - EMPTYLINES, 1);

            post_menu(mainMenu);

            if (hook_item >= menuSize - 1) {
                hook_item = menuSize - 2;
            }
            if (hook_item < 0) {
                hook_item = 0;
            }
            if (hook_row >= menuSize - rows + EMPTYLINES) {
                hook_row = std::max(0, menuSize - rows + EMPTYLINES - 1);
            }
            if (hook_row < 0) {
                hook_row = 0;
            }
            // In case it might be needed - the number of cursor line and the number 
            // of the top line will be displayed in the top left corner
            //mvprintw(0, 0, "%d", hook_item);
            //mvprintw(1, 0, "%d", hook_row); 
            set_top_row(mainMenu, hook_row);
            set_current_item(mainMenu, menuProcesses[hook_item]);

            wrefresh(menuscr);

        } else if (ch != ERR) {
            bool exit = false;

/*          -----------------------------------
            Proceed user activity
            -----------------------------------
*/
            if (ch != 'k') {
                killConfirm = false;
            }

            int hook_item = 0;
            switch (ch) {
                case 'k':
                    if (!killConfirm) {
                        killConfirm = true;
                    } else {
                        hook_item = item_index(current_item(mainMenu));
                        if (hook_item < static_cast <int> (processes.size())) {
                            kill(processes[hook_item].id, SIGTERM);
                            needsRefreshing = true;
                        }
                    }
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                    if (static_cast <int> (ch) - 48 != sortProcesses) {
                        sortProcesses = static_cast <int> (ch) - 48;
                    } else {
                        sortProcesses *= -1;
                    }
                    needsRefreshing = true;
                    break;
                case KEY_DOWN:
                    menu_driver(mainMenu, REQ_DOWN_ITEM);
                    break;
                case KEY_UP:
                    menu_driver(mainMenu, REQ_UP_ITEM);
                    break;
                case KEY_NPAGE:
                    menu_driver(mainMenu, REQ_SCR_DPAGE);
                    break;
                case KEY_PPAGE:
                    menu_driver(mainMenu, REQ_SCR_UPAGE);
                    break;
                case ' ':
                    hook_item = item_index(current_item(mainMenu));
                    if (hook_item < static_cast <int> (processes.size())) {
                        if (processes[hook_item].status == "T") {
                            kill(processes[hook_item].id, SIGCONT);
                        } else {
                            kill(processes[hook_item].id, SIGSTOP);
                        }
                        needsRefreshing = true;
                    }
                    break;
                case 'q':
                case KEY_F(1):
                    exit = true;
                    break;
            }
            wrefresh(menuscr);
            ++curComInRow;
            if (exit) break;
        }
    }
    
/*  -----------------------------------
    Clearing memory and returning back to the terminal window
    -----------------------------------
*/
    unpost_menu(mainMenu);
    free_menu(mainMenu);
    for (auto elem: menuProcesses) {
        free_item(elem);
    }
    wborder(stdscr, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    delwin(menuscr);
    delwin(stdscr);
    endwin();
}


