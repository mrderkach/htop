#include <algorithm>
#include <csignal>
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <ncurses.h>
#include <menu.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <vector>

class Process {
public:
    int id;
    std::string name;
    std::string status;
    int virt;
    int rss;
    std::string display;

    Process(int id, std::string name, std::string status, int virt, int rss, std::string display);
};

class SortProcesses {
public:
    int sortBy;

    SortProcesses(int sortBy);

    bool operator() (const Process &self, const Process &other);
};

void DrawBorder(WINDOW *win, int row, int column);

std::vector <Process> RefreshProcesses();

void Start();

