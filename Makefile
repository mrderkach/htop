CXX = g++
CXXFLAGS = -Wall -g -std=c++0x -Werror -O2 -lncurses -lmenu
LDFLAGS = -s
CXXFILES = $(wildcard ./src/*.cpp)
HFILES = $(wildcard ./src/*.h)
OBJECTS = $(CXXFILES:.cpp=.o)

all : htop

htop : $(OBJECTS)
	$(CXX) $^ $(CXXFLAGS) -o $@

include deps.make
deps.make: $(CXXFILES) $(HFILES)
	$(CXX) -MM $(CXXFILES) > deps.make

clean :
	rm -f ./src/*.o htop

